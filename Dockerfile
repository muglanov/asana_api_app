# Dockerfile
FROM python:3.7

# Set work directory
RUN mkdir /code
WORKDIR /code
# Install dependencies
COPY requirements.txt /code
RUN pip install -r requirements.txt
COPY ./asana_api_app/ /code
COPY setup_project /code
COPY create_django_admin_user.py /code
