import os
from django.contrib.auth import get_user_model
from django.db.utils import IntegrityError
User = get_user_model()
try:
    username = os.environ['DJANGO_ADMIN_USERNAME']
    password = os.environ['DJANGO_ADMIN_PWD']
    User.objects.create_superuser(username, 'admin@myproject.com', password)
except IntegrityError:
    print('Пользователь админ уже создан')
