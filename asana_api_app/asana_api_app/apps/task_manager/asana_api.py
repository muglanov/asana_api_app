import os
import asana

from .utils import Singleton

# os.environ['ASANA_ACCESS_TOKEN'] = '1/1177313535624252:54af681843be650e89f1166bc18d44d9'


class AsanaApi(metaclass=Singleton):

    def __init__(self):
        self.client = asana.Client.access_token(os.environ['ASANA_ACCESS_TOKEN'])
        self.me = self.client.users.me()

    def create_project(self, name):
        new_project_id = None
        try:
            workspace_id = self.me['workspaces'][0]['gid']
            new_project = self.client.projects.create_in_workspace(workspace_id, {'name': name})
            new_project_id = new_project['gid']
        except Exception as e:
            print(e)
        finally:
            return new_project_id

    def change_project_by_gid(self, gid, new_name):
        result = False
        try:
            self.client.projects.update_project(gid, name=new_name)
            result = True
        except Exception as e:
            print(e)
        finally:
            return result

    def create_task(self, project_gid, name, description):
        new_task_id = None
        try:
            workspace_id = self.me['workspaces'][0]['gid']
            new_task = self.client.tasks.create_in_workspace(
                workspace_id,
                {
                    'name': name,
                    'notes': description,
                })
            self.client.tasks.add_project_for_task(new_task['gid'], {'project': project_gid})
            new_task_id = new_task['gid']
        except Exception as e:
            print(e)
        finally:
            return new_task_id

    def update_task_by_gid(self, gid, name, description):
        result = False
        try:
            self.client.tasks.update_task(gid, name=name, notes=description)
            result = True
        except Exception as e:
            print(e)
        finally:
            return result
