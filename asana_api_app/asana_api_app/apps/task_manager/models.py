from django.db import models

from .asana_api import AsanaApi


class Project(models.Model):
    project_id = models.AutoField(verbose_name='Идентификатор проекта', primary_key=True)
    project_name = models.CharField('Название проекта', max_length=256)
    asana_id = models.CharField('Id asana', max_length=256, blank=True, editable=False)

    def __str__(self):
        return self.project_name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        result = False
        if not self.project_id:
            new_project_id = AsanaApi().create_project(self.project_name)
            if new_project_id:
                self.asana_id = new_project_id
                result = True
        else:
            if AsanaApi().change_project_by_gid(self.asana_id, self.project_name):
                result = True
        if result:
            super().save(force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'


class User(models.Model):
    user_id = models.AutoField(verbose_name='Идентификатор пользователя', primary_key=True)
    username = models.CharField('Имя пользователя', max_length=100)
    asana_id = models.CharField('Id asana', max_length=256, blank=True, editable=False)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Task(models.Model):
    task_id = models.AutoField(verbose_name='Идентификатор задачи', primary_key=True)
    project = models.ForeignKey(verbose_name='Проект', to=Project, on_delete=models.CASCADE)
    task_name = models.CharField('Название задачи', max_length=256)
    task_description = models.TextField('Описание задачи')
    # task_implementer = models.ForeignKey(verbose_name='Исполнитель', to=User, on_delete=models.CASCADE)
    asana_id = models.CharField('Id asana', max_length=256, blank=True, editable=False)

    def __str__(self):
        return f'Задача №{self.task_id}'

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        result = False
        if not self.task_id:
            new_task_id = AsanaApi().create_task(self.project.asana_id, self.task_name, self.task_description)
            if new_task_id:
                self.asana_id = new_task_id
                result = True
        else:
            if AsanaApi().update_task_by_gid(self.asana_id, self.task_name, self.task_description):
                result = True
        if result:
            super().save(force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
